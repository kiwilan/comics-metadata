# comicinfo

- GitHub: <https://github.com/anansi-project/comicinfo>
- Original `ComicInfo.xml` specifications: <https://wiki.mobileread.com/wiki/ComicRack>
- Docs: <https://anansi-project.github.io/docs/comicinfo/intro>
- Example: <https://github.com/anansi-project/rfcs/issues/3>

> ComicRack can add metadata via a ComicInfo.xml file. This file is added directly by ComicRack by entering the data in the Info dialog box. The ComicRack article shows an example.

## ComicInfo.xml

Basic version of the `ComicInfo.xml` file:

```xml
<?xml version="1.0"?>
<ComicInfo xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Title>Le come-back du siècle</Title>
  <Series>Transmetropolitain</Series>
  <Number>1.0</Number>
  <Writer>Warren Ellis, Darick Robertson</Writer>
  <Publisher>Vertigo</Publisher>
  <LanguageISO>fr</LanguageISO>
</ComicInfo>
```

## Version 1.0

From <https://github.com/anansi-project/comicinfo/blob/main/schema/v1.0/ComicInfo.xsd>

```xml
<?xml version="1.0" encoding="utf-8"?>
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:element name="ComicInfo" nillable="true" type="ComicInfo"/>
    <xs:complexType name="ComicInfo">
        <xs:sequence>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Title" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Series" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Number" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="-1" name="Count" type="xs:int"/>
            <xs:element minOccurs="0" maxOccurs="1" default="-1" name="Volume" type="xs:int"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="AlternateSeries" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="AlternateNumber" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="-1" name="AlternateCount" type="xs:int"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Summary" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Notes" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="-1" name="Year" type="xs:int"/>
            <xs:element minOccurs="0" maxOccurs="1" default="-1" name="Month" type="xs:int"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Writer" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Penciller" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Inker" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Colorist" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Letterer" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="CoverArtist" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Editor" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Publisher" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Imprint" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Genre" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Web" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="0" name="PageCount" type="xs:int"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="LanguageISO" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Format" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="Unknown" name="BlackAndWhite" type="YesNo"/>
            <xs:element minOccurs="0" maxOccurs="1" default="Unknown" name="Manga" type="YesNo"/>
            <xs:element minOccurs="0" maxOccurs="1" name="Pages" type="ArrayOfComicPageInfo"/>
        </xs:sequence>
    </xs:complexType>
    <xs:simpleType name="YesNo">
        <xs:restriction base="xs:string">
            <xs:enumeration value="Unknown"/>
            <xs:enumeration value="No"/>
            <xs:enumeration value="Yes"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:complexType name="ArrayOfComicPageInfo">
        <xs:sequence>
            <xs:element minOccurs="0" maxOccurs="unbounded" name="Page" nillable="true" type="ComicPageInfo"/>
        </xs:sequence>
    </xs:complexType>
    <xs:complexType name="ComicPageInfo">
        <xs:attribute name="Image" type="xs:int" use="required"/>
        <xs:attribute default="Story" name="Type" type="ComicPageType"/>
        <xs:attribute default="false" name="DoublePage" type="xs:boolean"/>
        <xs:attribute default="0" name="ImageSize" type="xs:long"/>
        <xs:attribute default="" name="Key" type="xs:string"/>
        <xs:attribute default="-1" name="ImageWidth" type="xs:int"/>
        <xs:attribute default="-1" name="ImageHeight" type="xs:int"/>
    </xs:complexType>
    <xs:simpleType name="ComicPageType">
        <xs:list>
            <xs:simpleType>
                <xs:restriction base="xs:string">
                    <xs:enumeration value="FrontCover"/>
                    <xs:enumeration value="InnerCover"/>
                    <xs:enumeration value="Roundup"/>
                    <xs:enumeration value="Story"/>
                    <xs:enumeration value="Advertisement"/>
                    <xs:enumeration value="Editorial"/>
                    <xs:enumeration value="Letters"/>
                    <xs:enumeration value="Preview"/>
                    <xs:enumeration value="BackCover"/>
                    <xs:enumeration value="Other"/>
                    <xs:enumeration value="Deleted"/>
                </xs:restriction>
            </xs:simpleType>
        </xs:list>
    </xs:simpleType>
</xs:schema>
```

### Example

```xml
<?xml version="1.0"?>
<ComicInfo xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Title>You Had One Job</Title>
  <Series>Fantastic Four</Series>
  <Number>22</Number>
  <Volume>2018</Volume>
  <AlternateSeries>Empyre</AlternateSeries>
  <SeriesGroup>Fantastic Four</SeriesGroup>
  <Summary>THE RETURN OF THE NEW FANTASTIC FOUR?! The events of EMPYRE threaten all life on Earth and the future balance of power throughout the cosmos! And the secret mission that the FF have placed into Valeria and Franklinâ€™s hands requires some big-league help!</Summary>
  <Notes>Tagged with the ninjas.walk.alone fork of ComicTagger 1.3.3 using info from Comic Vine on 2020-08-06 11:23:02.  [CVDB787351]
Scraped metadata from Comixology [CMXDB844632]</Notes>
  <Year>2020</Year>
  <Month>10</Month>
  <Day>1</Day>
  <Writer>Dan Slott</Writer>
  <Penciller>Paco Medina, Sean Izaakse</Penciller>
  <Inker>Paco Medina, Sean Izaakse</Inker>
  <Colorist>Jesus Aburtov, Marcio Menyz</Colorist>
  <Letterer>Joe Caramagna</Letterer>
  <CoverArtist>Espen Grundetjern, Iban Coello, John Rauch, Nick Bradshaw</CoverArtist>
  <Editor>Alanna Smith, Martin Biro, Tom Brevoort</Editor>
  <Publisher>Marvel</Publisher>
  <Genre>Superhero</Genre>
  <Web>https://comicvine.gamespot.com/fantastic-four-22-you-had-one-job/4000-787351/</Web>
  <PageCount>24</PageCount>
  <LanguageISO>en</LanguageISO>
  <AgeRating>9+ Only</AgeRating>
  <Characters>Alicia Masters, Captain Marvel, Franklin Richards, Human Torch, Invisible Woman, Jo-Venn, Mr. Fantastic, N'kalla, Sky, Spider-Man, Thing, Valeria Richards, Wolverine</Characters>
  <Teams>Cotati, Fantastic Four, Kree, New Fantastic Four, Skrulls</Teams>
  <ScanInformation>Zone-Empire</ScanInformation>
  <Pages>
    <Page Image="0" ImageSize="1833151" ImageWidth="1988" ImageHeight="3056" Type="FrontCover" />
    <Page Image="1" ImageSize="759685" />
    <Page Image="2" ImageSize="1512273" />
    <Page Image="3" ImageSize="1744159" />
    <Page Image="4" ImageSize="2231538" />
    <Page Image="5" ImageSize="1368429" />
    <Page Image="6" ImageSize="2192419" />
    <Page Image="7" ImageSize="1896222" />
    <Page Image="8" ImageSize="1920578" />
    <Page Image="9" ImageSize="1790033" />
    <Page Image="10" ImageSize="1883198" />
    <Page Image="11" ImageSize="2125301" />
    <Page Image="12" ImageSize="2057496" />
    <Page Image="13" ImageSize="1487550" />
    <Page Image="14" ImageSize="3230017" />
    <Page Image="15" ImageSize="3913563" />
    <Page Image="16" ImageSize="1392762" />
    <Page Image="17" ImageSize="1361652" />
    <Page Image="18" ImageSize="1491465" />
    <Page Image="19" ImageSize="3472903" />
    <Page Image="20" ImageSize="1492856" />
    <Page Image="21" ImageSize="2104835" />
    <Page Image="22" ImageSize="2697168" />
    <Page Image="23" ImageSize="750766" />
  </Pages>
</ComicInfo>
```

## Version 2.0

From <https://github.com/anansi-project/comicinfo/blob/main/schema/v2.0/ComicInfo.xsd>

```xml
<?xml version="1.0" encoding="utf-8"?>
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:element name="ComicInfo" nillable="true" type="ComicInfo"/>
    <xs:complexType name="ComicInfo">
        <xs:sequence>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Title" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Series" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Number" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="-1" name="Count" type="xs:int"/>
            <xs:element minOccurs="0" maxOccurs="1" default="-1" name="Volume" type="xs:int"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="AlternateSeries" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="AlternateNumber" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="-1" name="AlternateCount" type="xs:int"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Summary" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Notes" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="-1" name="Year" type="xs:int"/>
            <xs:element minOccurs="0" maxOccurs="1" default="-1" name="Month" type="xs:int"/>
            <xs:element minOccurs="0" maxOccurs="1" default="-1" name="Day" type="xs:int"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Writer" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Penciller" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Inker" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Colorist" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Letterer" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="CoverArtist" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Editor" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Publisher" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Imprint" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Genre" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Web" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="0" name="PageCount" type="xs:int"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="LanguageISO" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Format" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="Unknown" name="BlackAndWhite" type="YesNo"/>
            <xs:element minOccurs="0" maxOccurs="1" default="Unknown" name="Manga" type="Manga"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Characters" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Teams" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Locations" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="ScanInformation" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="StoryArc" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="SeriesGroup" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="Unknown" name="AgeRating" type="AgeRating"/>
            <xs:element minOccurs="0" maxOccurs="1" name="Pages" type="ArrayOfComicPageInfo"/>
            <xs:element minOccurs="0" maxOccurs="1" name="CommunityRating" type="Rating"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="MainCharacterOrTeam" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" default="" name="Review" type="xs:string"/>
        </xs:sequence>
    </xs:complexType>
    <xs:simpleType name="YesNo">
        <xs:restriction base="xs:string">
            <xs:enumeration value="Unknown"/>
            <xs:enumeration value="No"/>
            <xs:enumeration value="Yes"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="Manga">
        <xs:restriction base="xs:string">
            <xs:enumeration value="Unknown"/>
            <xs:enumeration value="No"/>
            <xs:enumeration value="Yes"/>
            <xs:enumeration value="YesAndRightToLeft"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="Rating">
        <xs:restriction base="xs:decimal">
            <xs:minInclusive value="0"/>
            <xs:maxInclusive value="5"/>
            <xs:fractionDigits value="2"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="AgeRating">
        <xs:restriction base="xs:string">
            <xs:enumeration value="Unknown"/>
            <xs:enumeration value="Adults Only 18+"/>
            <xs:enumeration value="Early Childhood"/>
            <xs:enumeration value="Everyone"/>
            <xs:enumeration value="Everyone 10+"/>
            <xs:enumeration value="G"/>
            <xs:enumeration value="Kids to Adults"/>
            <xs:enumeration value="M"/>
            <xs:enumeration value="MA15+"/>
            <xs:enumeration value="Mature 17+"/>
            <xs:enumeration value="PG"/>
            <xs:enumeration value="R18+"/>
            <xs:enumeration value="Rating Pending"/>
            <xs:enumeration value="Teen"/>
            <xs:enumeration value="X18+"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:complexType name="ArrayOfComicPageInfo">
        <xs:sequence>
            <xs:element minOccurs="0" maxOccurs="unbounded" name="Page" nillable="true" type="ComicPageInfo"/>
        </xs:sequence>
    </xs:complexType>
    <xs:complexType name="ComicPageInfo">
        <xs:attribute name="Image" type="xs:int" use="required"/>
        <xs:attribute default="Story" name="Type" type="ComicPageType"/>
        <xs:attribute default="false" name="DoublePage" type="xs:boolean"/>
        <xs:attribute default="0" name="ImageSize" type="xs:long"/>
        <xs:attribute default="" name="Key" type="xs:string"/>
        <xs:attribute default="" name="Bookmark" type="xs:string"/>
        <xs:attribute default="-1" name="ImageWidth" type="xs:int"/>
        <xs:attribute default="-1" name="ImageHeight" type="xs:int"/>
    </xs:complexType>
    <xs:simpleType name="ComicPageType">
        <xs:list>
            <xs:simpleType>
                <xs:restriction base="xs:string">
                    <xs:enumeration value="FrontCover"/>
                    <xs:enumeration value="InnerCover"/>
                    <xs:enumeration value="Roundup"/>
                    <xs:enumeration value="Story"/>
                    <xs:enumeration value="Advertisement"/>
                    <xs:enumeration value="Editorial"/>
                    <xs:enumeration value="Letters"/>
                    <xs:enumeration value="Preview"/>
                    <xs:enumeration value="BackCover"/>
                    <xs:enumeration value="Other"/>
                    <xs:enumeration value="Deleted"/>
                </xs:restriction>
            </xs:simpleType>
        </xs:list>
    </xs:simpleType>
</xs:schema>
```
