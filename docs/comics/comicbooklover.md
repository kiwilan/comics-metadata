# ComicBookLover

This software is totally abandoned and not supported anymore.

```json
{
  "appID": "ComicBookLover/1529",
  "lastModified": "2014-02-23 09:04:06 +0000",
  "ComicBookInfo/1.0": {
    "rating": 0,
    "credits": [
      { "primary": true, "role": "Writer", "person": "Edgar Rice Burroughs" }
    ],
    "issue": "1",
    "series": "The Martian",
    "tags": ["John Carter"],
    "title": "The Martian - A Princess of Mars (The Sun 1958)",
    "publisher": "The Sun",
    "publicationMonth": 1,
    "language": "English",
    "country": "United States",
    "publicationYear": 1958,
    "genre": "Sci-Fi"
  }
}
```
