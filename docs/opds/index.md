# OPDS

Open Publication Distribution System

- [opds.io](https://opds.io/)
- Wikipedia: <https://en.wikipedia.org/wiki/OPDS>
- Docs: <https://anansi-project.github.io/docs/opds-pse/intro>
- Example: <http://feedbooks.github.io/opds-test-catalog/catalog/root.xml>
- Samples:
  - <https://github.com/bmaupin/epub-samples>
  - <https://github.com/IDPF/epub3-samples>
- OPDS PSE: <https://anansi-project.github.io/docs/opds-pse/intro>

## Version 1.2

- Specification: <https://specs.opds.io/opds-1.2>

## Version 2.0 (draft)

- Specification: <https://drafts.opds.io/opds-2.0>
