# eBooks and Comics metadata

Documentation about eBooks and Comics metadata.

- [eBooks](https://gitlab.com/kiwilan/comics-metadata/-/blob/main/docs/ebooks/index.md): EPUB 2.0, EPUB 3.0, EPUB 3.2
- [Comics](https://gitlab.com/kiwilan/comics-metadata/-/blob/main/docs/comics/index.md): CBZ, CBR, CB7, CBT
- [OPDS](https://gitlab.com/kiwilan/comics-metadata/-/blob/main/docs/opds/index.md): OPDS 1.2, OPDS 2.0

## Softwares

- [comictagger](https://github.com/comictagger/comictagger): A multi-platform app for writing metadata to digital comics
- [Manga-Manager](https://github.com/MangaManagerORG/Manga-Manager): Manga Manager collects useful tools to make managing your manga library easy. (Acts directly on files)
- [Calibre](https://calibre-ebook.com/): calibre is a powerful and easy to use e-book manager.
  - [EmbedComicMetadata](https://github.com/dickloraine/EmbedComicMetadata): A Calibre Plugin to embed calibres metadata into cbz comic archieves
- [komga](https://github.com/gotson/komga): Media server for comics/mangas/BDs with API and OPDS support
- [Kavita](https://github.com/Kareadita/Kavita): Kavita is a fast, feature rich, cross platform reading server. Built with a focus for manga and the goal of being a full solution for all your reading needs. Setup your own server and share your reading collection with your friends and family.
- [codex](https://github.com/ajslater/codex): Codex is a web based comic archive browser and reader
- [cops](https://github.com/seblucas/cops): Calibre OPDS (and HTML) PHP Server : web-based light alternative to Calibre content server / Calibre2OPDS to serve ebooks (epub, mobi, pdf, ...)
- [koreader](https://github.com/koreader/koreader): An ebook reader application supporting PDF, DjVu, EPUB, FB2 and many more formats, running on Cervantes, Kindle, Kobo, PocketBook and Android devices

## API

- [Comic Vine](https://comicvine.gamespot.com/): Comic Vine is the largest comic book wiki in the universe
