# ACBF

From <https://launchpad.net/acbf>

> Advanced Comic Book Format besides comic book metadata (authors, genres, publisher, publish date, annotation ...) supports also definition of frames/panels (for panel by panel viewing), text-layers for translations, storing of table of contents and more. ACBF document (_.acbf) can be embedded into comic book archive (CBZ) or exist as a individual _.acbf file with binary data included. See the article on ACBF for more on this, including an example.

Advanced Comic Book Format (ACBF) specification is a distribution and interchange format for digital comic books. ACBF specification defines a means of representing structured and semanticaly enhanced content used in comic books. In contrast to widely used Comic Book Archive (CBR, CBZ …), content is represented in separate graphic and text layer(s) as well as comic book metadata is present.

Some of the properties of ACBF are:

- Definition of comic book metadata (title, authors, genres ...)
- Definition of comic book structure (pages, frames) as well as order in which they follow
- Indexing of comic book pages (creation of Table of Contents upon page title)
- Definition of text layer separate from background graphics as well as reading order of text areas in it
- Definition of semantics on the text layer (e.g. emphasis, code, speech, commentary …)
- Support for more text layers in one document (translations to different languages)

ACBF aims to be used as a digital comic book format that is able to properly represent any comic books content and at the same time be suitable for automatic processing, indexing, comic book collection management and conversion into other formats.

ACBF is an extension of XML. Some inspiration regarding meta-data and semantics of the text layer comes from Fictionbook (file format that is used for electronic books distribution) and ACV 2 (comic book format used by Droid Comic Viewer).

ACBF does not use any kind of DRM (Digital Rights Management) and all specifications are open.

PPA repository is available at: <https://launchpad.net/~acbf-development-team/+archive/ubuntu/acbf>
Free comic books in ACBF format can be downloaded from: <http://acbf.info/>

## Specifications

From <https://acbf.fandom.com/wiki/ACBF_Specifications>

ACBF is an extension of XML version 1.0. The entire document is enclosed in <ACBF> element and uses just its own ACBF namespace. Meta-data section contains the generic information about the comic book (title, author, publish date etc.) as well as publishing information (publisher, publish date) and information about particular ACBF document (document author, creation date …).

Body section contains the comic book content, including definition of pages, frames, (links to) images and text layers.

At the end of the document in the data section there are <binary> elements where all images and other binary data used in the entire document may be stored. These images are converted to BASE64.

```xml
<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/css" href="default.css"?>
<ACBF xmlns="http://www.acbf.info/xml/acbf/1.1">
 <meta-data>
  <book-info>
    ...
  </book-info>
  <publish-info>
    ...
  </publish-info>
  <document-info>
   ...
  </document-info>
 </meta-data>
 <body>
  <page>
   <title lang="en">Chapter 1: How it All Began</title>
   <image href="#page1.jpg"/>
   <text-layer lang="en">
    ...
   </text-layer>
   <frame points="10,75 650,137 650,562 10,562"/>
  </page>
 </body>
 <references></references>
 <data>
   <binary id="page1.jpg" content-type="image/jpeg">BASE64string</binary>
 </data>
</ACBF>
```

Stylesheet declaration can be specified using link to external css document or inside a `<style>` element. If no styles are defined as part of the document viewer application will render text layers with its defined default styles. More on styles see chapter on Stylesheet Declaration.
