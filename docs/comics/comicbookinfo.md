# comicbookinfo

> Another format is the ComicBookInfo metadata, which is stored directly in the ZIP file header as comment. The data is apparently stringified JSON data. Calibre can read this format. Here is an example for the format: https://code.google.com/p/comicbookinfo/wiki/Example

These recommendations are based on `comicbookinfo` format developed by Google and archived now on <https://code.google.com/archive/p/comicbookinfo/>.

ComicBookInfo is a metadata container format to be used with the CBZ digital comic file format.

## Overview

ComicBookInfo is a metadata container format to be used with the CBZ digital comic file format. It allows information such as the title, series, genre, publisher, author and other information to be stored in the file itself.

The combination of ComicBookInfo and CBZ provides a cross-platform solution for including metadata with digital comics, and thus allowing the tagging of digital comics.

## Metadata for Digital Comics

### How can we make comic book metadata available on a cross-platform basis, for existing and new digital comic files?

For the popular MP3 format, a chunk of data is added to the end (ID3v1) or inserted at the start (ID3v2) of an MP3 file. Adding data to the start or end of a CBZ or CBR digital comic would result in standard unzip and unrar tools no longer recognising the file as a compressed archive, and all existing digital comic readers would no longer be able to open the files.

Adding metadata to the CBZ or CBR digital comic, by inserting a file into the compressed ZIP / RAR archive, is suitable for new digital comic files. The EPUB format (based on ZIP) for digital books is proposing to have a rights.xml file stored in the root of the ZIP container. Doing something similar such as inserting a metadata file in the root of the comic archive would be costly for existing comics - they would need to be decompressed and recompressed in order to insert the metadata file, and upon any changes to the metadata.

The proposed solution is to add metadata to the ZIP comment of a CBZ digital comic. Updates and changes to the ZIP comment are computationally cheap O(1). Decompression and recompression of the CBZ archive is not required as the ZIP specification stores the UTF8 file comment at the end of the ZIP file. The result is that metadata can be read and updated on any computer platform which has ZIP support. The ZIP comment is currently unused for CBZ digital comics (nobody has declared an official use for it, until now).

### ComicBookInfo Metadata Format

ComicBookInfo metadata format: \* JSON (JavaScript Object Notation) \* UTF-8 text encoding

Metadata storage method: \* Stored inside the zip comment of a CBZ file

Maximum size of metadata: \* 65535 bytes \* unzip.h declares:

> `uLong size_file_comment; /* file comment length 2 bytes */`

## Using ComicBookInfo with non-CBZ comics

The proposed solution advocates embedding ComicBookInfo metadata in the zip comment of CBZ digital comics.

The de facto standard for distributing digital comics has been compressed archives, primarily the CBZ and CBR file formats, which are simply a folder of images, compressed with ZIP or RAR respectively.

RAR's compression algorithm is proprietary and a closed format, making it difficult for software developers to add RAR archive creation and manipulation at low-cost (one needs to apply and pay for a license). How to embed ComicBookInfo metadata in the comment section of CBR digital comics is unknown and unlikely to be easily implemented. In contrast, ZIP is an open format, freely available across all major operating systems, with source code available.

Software developers on Mac, Linux, Windows and other platforms are free to add ComicBookInfo support to CBR and PDF comics, as they see fit, although the results may not be cross-platform.

On Mac OS X, ComicBookLover supports metadata for CBZ, CBR and PDF comics by making use of extended attributes, a feature of the system's default HFS+ file-system. Spotlight can index the metadata stored in the extended attributes, thus allowing users to search digital comics for metadata as easily as text or PDF documents. However, only CBZ comics will have the metadata embedded in the file itself, allowing the comic's metadata to be examined and updated by other comic reader applications on other operating systems.

## Other comic book metadata implementations

- [Comic Book Markup Language](http://www.cbml.org/) (Though this is more appropriate for marking up full comics, rather than just information about the comics)
- [comicbook.xml](http://www.rasalghul.co.uk/cbr/#Metadata)
- ComicInfo.xml (from Comic Rack)

## Example ComicBookInfo metadata

```json
{
  "appID": "ComicBookLover/888",
  "lastModified": "2009-10-25 14:51:31 +0000",
  "ComicBookInfo/1.0": {
    "series": "Watchmen",
    "title": "At Midnight, All the Agents",
    "publisher": "DC Comics",
    "publicationMonth": 9,
    "publicationYear": 1986,
    "issue": 1,
    "numberOfIssues": 12,
    "volume": 1,
    "numberOfVolumes": 1,
    "rating": 5,
    "genre": "Superhero",
    "language": "English",
    "country": "United States",
    "credits": [
      {
        "person": "Moore, Alan",
        "role": "Writer",
        "primary": "YES"
      },
      {
        "person": "Gibbons, Dave",
        "role": "Artist",
        "primary": "YES"
      },
      {
        "person": "Gibbons, Dave",
        "role": "Letterer"
      },
      {
        "person": "Gibbons, John",
        "role": "Colorer"
      },
      {
        "person": "Wein, Len",
        "role": "Editor"
      },
      {
        "person": "Kesel, Barbara",
        "role": "Editor"
      }
    ],
    "tags": ["Rorschach", "Ozymandias", "Nite Owl"],
    "comments": "Tales of the Black Freighter..."
  },
  "x-ComicBookLover": {
    "key": "value"
  },
  "x-LinuxComicReader": {
    "key": "value"
  }
}
```
