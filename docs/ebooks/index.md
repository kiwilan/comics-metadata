# eBooks Metadata for EPUB

- W3C EPUB 3.2 specs: <https://www.w3.org/publishing/epub3/epub-spec.html>
- EPUB 3.0 International Digital Publishing Forum (IDPF): <https://idpf.org/epub/30/spec/epub30-overview.html>
- EPUB 3.2: <https://w3c.github.io/epub-specs/archive/epub32/spec/epub-changes.html>
