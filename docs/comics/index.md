# Comics Metadata for CBA

> A CBA, comic book archive, or comic book reader file (also called sequential image file) is a type of archive file for the purpose of sequential viewing of images, commonly for comic books. Comic book archive is not a distinct file format. It is a filename extension naming convention.
>
> The filename extension indicates the archive type used:
>
> - `.cb7` → `7z`
> - `.cba` → `ACE`
> - `.cbr` → `RAR`
> - `.cbt` → `TAR`
> - `.cbz` → `ZIP`
>
> Comic book archive files mainly consist of a series of image files with specific naming, typically PNG (lossless compression) or JPEG (lossy compression, not JPEG-LS or JPEG XT) files, stored as a single archive file. Occasionally GIF, BMP, and TIFF files are seen. Folders may be used to group images in a more logical layout within the archive, like book chapters.
>
> Comic book archive viewers typically offer various dedicated functions to read the content, like one page forward/backwards, go to first/last page, zoom or print. Some applications support additional tag information in the form of embedded XML files in the archive or use of the ZIP comment to store additional information. These files can include additional information like artists, story information, table of contents or even a separate text layer for comic book translations.

To know more about CBA, check [Wikipedia page](https://en.wikipedia.org/wiki/Comic_book_archive).

You can find an exhaustive list of informations about CBA (Comic Book Archive) on [mobileread](https://wiki.mobileread.com/wiki/CBR_and_CBZ#Metadata). More about [metadata](https://wiki.mobileread.com/wiki/Metadata).

## Specifications

> Normally a CBR or CBZ file does not have metadata, there are no text files and the images are viewed in alphabetical order. While there is no standard for metadata there are several methods to store comic book metadata in comic book archives.

- [comicinfo](https://gitlab.com/kiwilan/comics-metadata/-/blob/main/docs/comics/comicinfo.md) by [anansi-project](https://github.com/anansi-project) (based on `ComicInfo.xml` by ComicRack) _active development_
- [acbf](https://gitlab.com/kiwilan/comics-metadata/-/blob/main/docs/comics/acbf.md) by [Robert Kubík](https://launchpad.net/acbf) (Pastierovič) _active development_
- [comicbooklover](https://gitlab.com/kiwilan/comics-metadata/-/blob/main/docs/comics/comicbooklover.md) _not under active development_
- [comicbookinfo](https://gitlab.com/kiwilan/comics-metadata/-/blob/main/docs/comics/comicbookinfo.md) by [Google](https://code.google.com/archive/p/comicbookinfo/) _not under active development_
